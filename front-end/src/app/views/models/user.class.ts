export class User
{
    id: number;
    username: String;
    name: String;
    birth: string;
    email: String;
    department_id: String;
    is_manager:number;
    position:String;
    taxcode:String;
    salary:number;
}