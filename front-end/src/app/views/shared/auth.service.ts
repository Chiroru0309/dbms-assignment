import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

export interface LoginInfo{
  userName: string;
  passWord: string;
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedInStatus =JSON.parse(localStorage.getItem('loggedIn')|| 'false');
  constructor( private http: HttpClient) { 
  }

  public ten:String;
  public matkhau:String;
  setLoggedIn( value: boolean)
  {
    this.loggedInStatus = value;
    localStorage.setItem('loggedIn',value.toString());
  }
  get isLoggedIn()
  {
    return JSON.parse(localStorage.getItem('loggedIn')||this.loggedInStatus.toString());
  }
  login(loginInfo: LoginInfo)
  {
      this.ten = loginInfo.userName;
      this.matkhau = loginInfo.passWord;
      if ((this.ten == 'hr' && this.matkhau=='123456') || (this.ten=='user1' && this.matkhau=='user1'))
    return this.http.post('http://5d4a5a4f5c331e00148eb0bb.mockapi.io/nguoisudung', loginInfo,{
      observe:"body"
    })
    else return null;
  }
}
