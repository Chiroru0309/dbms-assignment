import { Injectable } from '@angular/core';
import {Http, Response,  Headers, RequestOptions, RequestMethod} from '@angular/http';
import {BinhLuan} from '../models/BinhLuan.class';
import {map} from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class BinhLuanService{
    constructor(public http:Http){}
    getBinhLuans()
    {
        return this.http.get('http://localhost/api/product/readBinhLuan.php');
    }
    postBinhLuans(bluan:BinhLuan): Observable<BinhLuan>
    {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('http://localhost/api/product/createBinhLuan.php',bluan,options).pipe(map((res:Response)=>res.json()));
    }
}