const database = require('../services/database.js');
const oracledb = require('oracledb');
const baseQuery =
//  `select id "id",
//     first_name "first_name",
//     last_name "last_name",
//     email "email",
//     phone_number "phone_number",
//     hire_date "hire_date",
//     job_id "job_id",
//     salary "salary",
//     commission_pct "commission_pct",
//     manager_id "manager_id",
//     department_id "department_id"
//   from employees`;
`SELECT
id "id",
username "username",
name "name",
birth "birth",
email "email",
department_id "department_id",
position "position",
is_manager "is_manager",
taxcode "taxcode",
salary "salary"
  from employee `;
  async function find(context) {
    let query = baseQuery;
    const binds = {};
  
    if (context.id) {
      binds.employee_id = context.id;
  
      query += `\nwhere employee_id = :employee_id`;
    }
  
    const result = await database.simpleExecute(query, binds);
  
    return result.rows;
  }
  
  module.exports.find = find;

const createSql =
 `insert into employee (
    
    username,
    name,
    email,
    salary,
    birth,
    position,
    is_manager
    taxcode,
    department_id
  ) values (
    
    :username,
    :name,
    :email,
    :salary,
    :birth,
    :position,
    :is_manager,
    :taxcode,
    :department_id
  );`;

async function create(emp) {
  const employee = Object.assign({}, emp);

  employee.employee_id = {
    dir: oracledb.BIND_OUT,
    type: oracledb.NUMBER
  }

  const result = await database.simpleExecute(createSql, employee);

  employee.employee_id = result.outBinds.employee_id[0];

  return employee;
}

module.exports.create = create;

const updateSql =
 `update employee

 set 
     name = :name,
    username = :username,
    email = :email,
    taxcode = :taxcode,
    birth = :birth,
    position = :position,
    salary = :salary,
    is_manager = :is_manager,
    department_id = :department_id
  where id = :employee_id`;
 
async function update(emp) {
  const employee = Object.assign({}, emp);
  const result = await database.simpleExecute(updateSql, employee);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return employee;
  } else {
    return null;
  }
}
 
module.exports.update = update;