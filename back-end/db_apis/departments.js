const database = require('../services/database.js');

const baseQuery =
`SELECT
dep.id "id",
dep.name "name"
  from department dep`;
async function find(context) {
  let query = baseQuery;
  const binds = {};

  if (context.id) {
    binds.department_id = context.id;

     query += `\nwhere department_id = :department_id`;
    // query += `\n`;
  }

  const result = await database.simpleExecute(query, binds);

  return result.rows;
}

module.exports.find = find;
