const departments = require('../db_apis/departments.js');

async function get(req, res, next) {
  try {
    
    const context = {};

    context.id = parseInt(req.params.id, 10);

    const rows = await departments.find(context);

    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).jsonp(rows[0]);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(200).jsonp(rows);
    }
  } catch (err) {
    next(err);
  }
}

module.exports.get = get;
