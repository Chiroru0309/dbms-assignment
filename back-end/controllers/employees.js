const employees = require('../db_apis/employees.js');

async function get(req, res, next) {
  try {
    
    const context = {};

    context.id = parseInt(req.params.id, 10);

    const rows = await employees.find(context);

    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).jsonp(rows[0]);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(200).jsonp(rows);
    }
  } catch (err) {
    next(err);
  }
}


module.exports.get = get;

function getEmployeeFromRec(req) {
  const employee = {
    //  id: req.body.id,
    name: req.body.name,
    username: req.body.username,
    email: req.body.email,
    salary: req.body.salary,
    birth: req.body.birth,
    department_id: req.body.department_id,
    taxcode: req.body.taxcode,
    position: req.body.position,
    is_manager: req.body.is_manager
  };
 
  return employee;
}

async function post(req, res, next) {
  try {
    let employee = getEmployeeFromRec(req);
    res.setHeader('Content-Type', 'application/json');
    employee = await employees.create(employee);
 
    res.status(201).json(employee);
  } catch (err) {
    next(err);
  }
}
 
module.exports.post = post;

async function put(req, res, next) {
  try {
    let employee = getEmployeeFromRec(req);
    res.setHeader('Content-Type', 'application/json');
    employee.id = parseInt(req.params.id, 10);
 
    employee = await employees.update(employee);
 
    if (employee !== null) {
      res.status(200).json(employee);
    } else {
      res.status(404).end();
    }
  } catch (err) {
    next(err);
  }
}
 
module.exports.put = put;