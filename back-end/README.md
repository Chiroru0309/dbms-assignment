# Setup

Create ```.env``` file, the example of this file is ```.env.example```.

# Run

```bash
docker-compose up -d
```

# Logs

```bash
docker logs back-end_node_1
```

# Update new code

```bash
docker-compose build && docker-compose up -d
```