const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  hrPool: {
    user: process.env.USER,
    password: process.env.PASSWORD,
    connectString: process.env.CONNECT_STRING,
    poolMin: 10,
    poolMax: 10,
    poolIncrement: 0
  }
};
