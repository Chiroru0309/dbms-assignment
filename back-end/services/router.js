const express = require('express');
const router = new express.Router();
const employees = require('../controllers/employees.js');
const departments = require('../controllers/departments.js');

router.route('/employees/:id?')
  .get(employees.get)
  .post(employees.post)
  .put(employees.put);
  // .delete(employees.delete);

router.route('/departments/:id?')
  .get(departments.get);
  // .post(departments.post)
  // .put(departments.put)
  // .delete(departments.delete);
module.exports = router;
